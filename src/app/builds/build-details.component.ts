import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { DataService } from '../data.service';
import { Build, Job } from '../models'; 

@Component({
  selector: 'app-build-details',
  templateUrl: './build-details.component.html'
})
export class BuildDetailsComponent implements OnInit {
  
  //id: number;
  //private sub: any;
  build: Build;
  builds: Build[];
  jobs: Job[];

  constructor(
    private route: ActivatedRoute, 
    private dataService: DataService
  ) { }

  getJobsList(): void {
    //this.dataService.getJobsList(this.build.jobs).then(jobs => this.jobs = jobs);
  }

  getBuild() : any {
    //this.dataService.getBuilds().then(builds => this.builds = builds);
    this.dataService.getBuild(1).subscribe(res => console.log(res));
    return this.dataService.getBuild(1).subscribe(builds => this.builds = builds);
  }

  ngOnInit(): void {
    //this.dataService.getBuild(0).subscribe(res => console.log(res));
  
  /*
    this.route.params
      .switchMap((params: Params) => this.dataService.getBuild(+params['id']))
      .subscribe(
        builds => this.build = builds
      );
    */

    //this.getJobsList();
    this.getBuild();
    //console.log(this.build);
  }

}
