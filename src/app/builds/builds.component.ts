import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { Build } from '../models'; 

@Component({
  selector: 'app-builds',
  templateUrl: './builds.component.html',
  styleUrls: ['./builds.component.scss']
})
export class BuildsComponent implements OnInit {

  builds: Build[];

  constructor(private dataService: DataService) { }
  
  getBuilds() : any {
    //this.dataService.getBuilds().then(builds => this.builds = builds);
    //this.dataService.getBuilds().subscribe(res => console.log(res));
    return this.dataService.getBuilds().subscribe(builds => this.builds = builds);
  }
  
  ngOnInit() {
    this.getBuilds();
  }
}