import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {

    let jobs = [
      //{id: 0, name: 'MJF Job', builds: [1]},
      {id: 1, name: 'job: Shoe soles from client', builds: []},
      {id: 2, name: 'job: A nice plastic car', builds: [1,2]},
      {id: 3, name: 'job: Another lorem ipsum job', builds: [3]},
      {id: 4, name: 'job: Something to print', builds: [3]},
      {id: 5, name: 'job: 250516 urgent from fav client', builds: [3]}
    ];

    let builds = [
      //{id: 0, name: 'MJF Job', jobs: []},
      {id: 1, name: 'Build: Shoe soles from client', jobs: [2]},
      {id: 2, name: 'Build: A nice plastic car', jobs: [2]},
      {id: 3, name: 'Build: This is a really huge build', jobs: [3,4,5]}
    ];

    return {jobs, builds};

  }
}