import { Component, Input, HostListener, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentFactory } from '@angular/core';
import {CutawayComponent} from '../tools/cutaway/cutaway.component';
import {DuplicateComponent} from '../tools/duplicate/duplicate.component';

@Component({
  selector: 'toolbar-item',
  templateUrl: './toolbar-item.component.html',
  // List the child elements in the entry components array
  entryComponents: [CutawayComponent, DuplicateComponent] 
})

// Add child components dynamicly based on
// http://raydaq.com/articles/dynamic-child-component-loader
// https://www.lucidchart.com/techblog/2016/07/19/building-angular-2-components-on-the-fly-a-dialog-box-example/

export class ToolbarItemComponent {

  @Input() toolbarItem;

  // Container for dynamic load of child components
  @ViewChild('panelContainer', {read: ViewContainerRef}) private panelContainer: ViewContainerRef;
  // Factory to ref child component
  private factory: ComponentFactory<any>;
  private factoryRef;

  private hasPanel:Boolean = false;
  private isEnabled:Boolean = false;
  
  iconSrc: String;
  iconFileType: String = '@2x.png';
  
  constructor (private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.panelContainer.clear();

    switch (this.toolbarItem.toolPanel) {
      case "duplicate":
        this.factory = this.componentFactoryResolver.resolveComponentFactory(DuplicateComponent);
        break;
      case "cutaway":
        this.factory = this.componentFactoryResolver.resolveComponentFactory(CutawayComponent);
        break;
    }

    if(this.factory) {
      this.hasPanel = true;
      this.factoryRef = this.panelContainer.createComponent(this.factory);
      // Listen for close event on panel
      this.factoryRef.instance.close.subscribe(() => {
        this.togglePanelState();
      })
    }
      
    // Set default icon
    this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Default${ this.iconFileType }`;
  }

  ngOnDestroy() {
    if (this.factoryRef) {
      this.factoryRef.destroy();
    }
  }

  onClick(event) {
    if(this.hasPanel){
      //this.factoryRef.instance.toggleVisibility();
      this.togglePanelState();
    } 
  }

  togglePanelState() {
    this.isEnabled = !this.isEnabled;
    this.factoryRef.instance.toggleVisibility();
  }

  onEnter(event) {
    this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Hover${ this.iconFileType }`;
  }

  onLeave(event) {
    this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Default${ this.iconFileType }`;
  }

  onDown(event) {
    this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Pressed${ this.iconFileType }`;
  }

  onUp(event) {
    this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Hover${ this.iconFileType }`;
  }

  /*
  @HostListener('mouseenter') onMouseEnter() {
      this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Hover${ this.iconFileType }`;
  }

  @HostListener('mouseleave') onMouseLeave() {
      this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Default${ this.iconFileType }`;
  }

  @HostListener('mousedown') onMouseDown() {
      this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Pressed${ this.iconFileType }`;
  }

  @HostListener('mouseup') onMouseUp() {
      this.iconSrc = `${ this.toolbarItem.iconBaseUrl }_Hover${ this.iconFileType }`;

      // Call child component somehow here
  }
  */

  setStyles() {
    let styles = {
      // CSS property names
      'background-image':  `url('${ this.iconSrc }')`
    };
    return styles;
  }
}