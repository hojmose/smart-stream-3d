import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    //console.log(this.toolbarList);
    if(!this.toolbarList) {
      this.toolbarList = "toolbarEditItems";
    }
    
    if(this.toolbarList == "toolbarEditItems") {
      this.toolArray = this.toolbarEditItems;
    } else if(this.toolbarList == "toolbarViewItems") {
      this.toolArray = this.toolbarViewItems;
    }
    
  }

  @Input('toolbarList') toolbarList: string;

  toolArray = [];

  toolbarEditItems = [
    {
      name: "Select",
      class: "select",
      iconBaseUrl: "assets/toolbar/Select",
      iconSrc: "assets/toolbar/Select_Default.svg",
      toolPanel: ""
    },
    {
      name: "Move",
      class: "move",
      iconBaseUrl: "assets/toolbar/Move",
      iconSrc: "assets/toolbar/Move_Default.svg",
      toolPanel: ""
    },
    {
      name: "Rotate",
      class: "rotate",
      iconBaseUrl: "assets/toolbar/Rotate",
      iconSrc: "assets/toolbar/Rotate_Default.svg",
      toolPanel: ""
    },
    {
      name: "Scale",
      class: "scale",
      iconBaseUrl: "assets/toolbar/Scale",
      iconSrc: "assets/toolbar/Scale_Default_Fixed_Sketch.svg",
      toolPanel: ""
    },
    {
      name: "Duplicate",
      class: "duplicate",
      iconBaseUrl: "assets/toolbar/Duplicate",
      iconSrc: "assets/toolbar/Duplicate_Default.svg",
      toolPanel: "duplicate"
    },
    {
      name: "Lay Flat",
      class: "lay-flat",
      iconBaseUrl: "assets/toolbar/Lay_flat",
      iconSrc: "assets/toolbar/Lay_flat_Default.svg",
      toolPanel: ""
    }
  ];

  toolbarViewItems = [
    {
      name: "Clipping plane",
      class: "clipping-plane",
      iconBaseUrl: "assets/toolbar/Clipping_plane_Front",
      iconSrc: "assets/toolbar/Clipping_plane_Front_Default.svg",
      toolPanel: "cutaway"
    },
    {
      name: "Zoom",
      class: "zoom",
      iconBaseUrl: "assets/toolbar/Zoom",
      iconSrc: "assets/toolbar/Zoom_Default.svg",
      toolPanel: ""
    },
    {
      name: "Pan",
      class: "pan",
      iconBaseUrl: "assets/toolbar/Pan",
      iconSrc: "assets/toolbar/Pan_Default.svg",
      toolPanel: ""
    },
    {
      name: "Orbit",
      class: "orbit",
      iconBaseUrl: "assets/toolbar/Orbit",
      iconSrc: "assets/toolbar/Orbit_Default.svg",
      toolPanel: ""
    },
    {
      name: "Home",
      class: "home",
      iconBaseUrl: "assets/toolbar/Home_view",
      iconSrc: "assets/toolbar/Home_view_Default.svg",
      toolPanel: ""
    },
    {
      name: "View angle",
      class: "view-angle",
      iconBaseUrl: "assets/toolbar/View_angle_Front",
      iconSrc: "assets/toolbar/View_angle_Front_Default.svg",
      toolPanel: ""
    }
  ];

}
