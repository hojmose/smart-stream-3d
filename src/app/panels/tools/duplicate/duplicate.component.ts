import { Component, OnInit, HostListener, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-duplicate',
  templateUrl: './duplicate.component.html',
  styleUrls: ['./duplicate.component.scss']
})
export class DuplicateComponent implements OnInit {

  private showPanel: Boolean = false;
  
  close = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  toggleVisibility() {
    // Toggle show hide panel
    this.showPanel = !this.showPanel;
  }

  onCloseClick() {
    this.close.emit('event');
  }
}