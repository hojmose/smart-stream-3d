import { Component, OnInit, HostListener, Input, EventEmitter,ElementRef } from '@angular/core';

@Component({
  selector: 'app-cutaway',
  templateUrl: './cutaway.component.html',
  styleUrls: ['./cutaway.component.scss']
})

export class CutawayComponent implements OnInit {

  private showPanel: Boolean = false;
  
  close = new EventEmitter();

  isDraggable: boolean = false;
  
  constructor(private element: ElementRef) {
    
    let style = element.nativeElement.style;
    style.position = "fixed";
    style.top = "360px";
    style.left = "20px";
    style["-webkit-transform"] = "translate(-50%, -50%)";
    style.transform = "translate(-50%, -50%)";
    
  }

  ngOnInit() {
  }

  draggable( isDraggable ){
    console.log("my-modal: isDraggable", isDraggable);
    this.isDraggable = isDraggable;

  }

  toggleVisibility() {
    // Toggle show hide panel
    this.showPanel = !this.showPanel;
  }

  onCloseClick() {
    this.close.emit('event');
  }
}
