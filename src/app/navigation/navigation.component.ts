import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})

/*
const NAV_ITEMS: Hero[] = [
  { title: 'Home', class: 'home', route: '/' },
  { title: 'Creator', class: 'creator', route: '/creator' },
  { title: 'Builds', class: 'builds', route: '/builds' },
  { title: 'Printers', class: 'printers', route: '/printers' }
];
*/

export class NavigationComponent implements OnInit {

  constructor(private location: Location) { }

  ngOnInit() {
  }

  menuState:string = 'in';
 
  toggleMenu() {
    // Toggle menu in/out states
    this.menuState = this.menuState === 'out' ? 'in' : 'out';
  }

  goBack(): void {
    this.location.back();
  }

  goForward(): void {
    this.location.forward();
  }

}