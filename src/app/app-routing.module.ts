import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CreatorComponent } from './creator/creator.component';
import { BuildDetailsComponent } from './builds/build-details.component';
import { BuildsComponent } from './builds/builds.component';
import { PrintersComponent } from './printers/printers.component';


const routes: Routes = [
    
    { path: 'creator', component: CreatorComponent },
    { path: 'builds/:id', component: BuildDetailsComponent },
    { path: 'builds', component: BuildsComponent },
    { path: 'printers', component: PrintersComponent },
    { path: '', component: HomeComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}