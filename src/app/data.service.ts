import { Job, Build } from './models';

import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/forkJoin';

// http://stackoverflow.com/questions/34405039/combining-promises-in-angular-2
// http://www.syntaxsuccess.com/viewarticle/angular-2.0-and-http
// http://restlet.com/blog/2016/04/12/interacting-efficiently-with-a-restful-service-with-angular2-and-rxjs-part-2/
// http://blog.danieleghidoli.it/2016/10/22/http-rxjs-observables-angular/
// https://xgrommx.github.io/rx-book/content/observable/observable_instance_methods/flatmap.html

@Injectable()
export class DataService {

  private baseUrl = 'app/';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

    getJobs(): Promise<Job[]> {
        return this.http.get(this.baseUrl + 'jobs')
        .toPromise()
        .then(response => response.json().data as Job[])
        .catch(this.handleError);
  }
  // Get jobs based on array of IDs, ie [2,4,5]
    getJobsList(ids: number[]): Promise<Job[]> {
        return this.getJobs()
            .then(jobs => jobs.filter(job => ids.indexOf(job.id) > 0)); // Not sure this is a good solution to filter jobs
    }
    /*
  getBuilds(): Promise<Build[]> {
        return this.http.get(this.baseUrl + 'builds')
        .toPromise()
        .then(response => response.json().data as Build[])
        .catch(this.handleError);
  }
  */
  
  getBuilds(): Observable<Build[]> {
    return this.http.get(this.baseUrl + 'builds')
      .map(response => response.json().data as Build)
      .catch(this.handleError);
  }
/*
  getBuild(id: number): Observable<Build[]> {
  
    return this.http.get(this.baseUrl + 'builds')
      .map(response => response.json().data as Build[])
      .catch(this.handleError);
  }
  */
  getBuild(id: number): Observable<any> {

    return this.http.get(this.baseUrl + 'builds/' + id)
        .map(response => response.json().data as Build)
        .flatMap((singleBuild: Build) => {
            return this.http.get(this.baseUrl + 'jobs')
                .map((response: any) => {
                    let jobs:any[] = response.json().data; // Can we do TS filtering here?
                    jobs.filter(job => (singleBuild.jobs.indexOf(job.id) > -1)); //singleBuild.jobs.indexOf(job.id)
                    console.log(singleBuild.jobs);
                    singleBuild.jobs = jobs;
                    return singleBuild;
                })
                //.filter(job => job.id === 1)
            })
        .catch(this.handleError);
  }
/*
    getBuild(id: number): Promise<Build> {
        return this.getBuilds()
            .then(builds => builds.find(build => build.id === id));
    }
*/
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}