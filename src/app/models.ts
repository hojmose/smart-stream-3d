// Job model
export class Job {
  id: number;
  name: string;
  builds: Number[];
}

// Build model
export class Build {
  id: number;
  name: string;
  jobs: Number[];
}