import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';
import { DataService } from './data.service';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HeaderComponent } from './header/header.component';
import { PrintersComponent } from './printers/printers.component';
import { BuildsComponent } from './builds/builds.component';
import { BuildDetailsComponent } from './builds/build-details.component';
import { CreatorComponent } from './creator/creator.component';
import { ToolbarComponent } from './panels/toolbar/toolbar.component';
import { ToolbarItemComponent } from './panels/toolbar/toolbar-item.component';
import { CutawayComponent } from './panels/tools/cutaway/cutaway.component';
import { DuplicateComponent } from './panels/tools/duplicate/duplicate.component';
import { Draggable } from './shared/draggable';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    HeaderComponent,
    PrintersComponent,
    BuildsComponent,
    BuildDetailsComponent,
    CreatorComponent,
    CreatorComponent,
    ToolbarComponent,
    ToolbarItemComponent,
    CutawayComponent,
    DuplicateComponent,
    Draggable
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
