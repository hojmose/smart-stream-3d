import { SmartStream3dPage } from './app.po';

describe('smart-stream-3d App', function() {
  let page: SmartStream3dPage;

  beforeEach(() => {
    page = new SmartStream3dPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
